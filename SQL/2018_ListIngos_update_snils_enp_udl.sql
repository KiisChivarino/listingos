/****** 2018_ListIngos_update_snils_enp_udl.sql  ******/

--UPDATE l SET snils=replace(replace(isNull(sp.ss,''),' ',''),'-','') /*
select l.snils,sp.ss,replace(replace(isNull(sp.ss,''),' ',''),'-','')as sss
--*/ 
from lists l
LEFT OUTER JOIN listPrepare lp on l.n_rec=lp.n_rec
LEFT OUTER JOIN srz..people sp on lp.pid=sp.ID
WHERE lp.err in(1,3)
/*
update ListPrepare set err=0,kontr=0,pid_snils=null where isNull(err,0)in(1,3)

UPDATE lp SET lp.pid_snils=ps.ID
FROM [ListIngos].[dbo].[ListPrepare] lp
INNER JOIN [ListIngos].[dbo].Lists l ON l.n_rec=lp.n_rec
LEFT OUTER JOIN srz..people ps ON ps.SS=(SUBSTRING(l.snils,1,3)+'-'+SUBSTRING(l.snils,4,3)+'-'+SUBSTRING(l.snils,7,3)+' '+SUBSTRING(l.snils,10,2))
WHERE lp.pid_snils is null and isNull(kontr,0)=0 and isNull(err,0)=0
--*/
/*
UPDATE lp SET lp.pid_enp=pe.ID
FROM [ListIngos].[dbo].[ListPrepare] lp
INNER JOIN [ListIngos].[dbo].Lists l ON l.n_rec=lp.n_rec
left outer join srz..people  pe on l.pnum=pe.enp
WHERE lp.pid_enp is null and isNull(kontr,0)=0 and isNull(err,0)=0
--*/
/*
UPDATE lp SET lp.pid_udl=pd.ID
FROM [ListIngos].[dbo].[ListPrepare] lp
INNER JOIN [ListIngos].[dbo].Lists l ON l.n_rec=lp.n_rec
left outer join srz..people pd on 
(CASE 
	 WHEN udldoctp='Паспорт РФ' THEN STUFF(REPLACE(udlser,' ',''),3,0,' ')
	 ELSE udlser
END+udlnum)=pd.DOCS+pd.DOCN
WHERE lp.pid_udl is null and isNull(kontr,0)=0 and isNull(err,0)=0
--*/
/*
UPDATE ListPrepare SET pid=
CASE 
WHEN (pid_enp IS NOT NULL AND pid_snils IS NOT NULL AND pid_enp=pid_snils) THEN pid_enp
WHEN (pid_udl IS NOT NULL AND pid_enp IS NOT NULL AND pid_enp=pid_udl) THEN pid_udl
WHEN (pid_snils IS NOT NULL AND pid_udl IS NOT NULL AND pid_snils=pid_udl) THEN pid_snils
ELSE NULL
END
where isNull(kontr,0)=0 and isNull(err,0)=0

update ListPrepare set err=1 where pid_enp IS NOT NULL AND pid_snils IS NOT NULL AND pid_enp<>pid_snils and isNull(kontr,0)=0

update ListPrepare set err=2 where pid_udl IS NOT NULL AND pid_enp IS NOT NULL AND pid_enp<>pid_udl and isNull(err,0)=0 and isNull(kontr,0)=0

update ListPrepare set err=3 where pid_snils IS NOT NULL AND pid_udl IS NOT NULL AND pid_snils<>pid_udl and isNull(err,0)=0 and isNull(kontr,0)=0

update ListPrepare set pid=isNull(pid_enp,isNull(pid_snils,isNull(pid_udl,0)))where isNull(err,0)=0 and isNull(err,0)=0 and isNull(kontr,0)=0

update ListPrepare set kontr=1 where isNull(pid,0)>0 and isNull(err,0)=0 and isNull(kontr,0)=0
--*/

select count(*)as err from listPrepare where err in(1,3)

--select * from listPrepare where err>0
