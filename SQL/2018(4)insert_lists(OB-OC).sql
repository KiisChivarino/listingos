use ListIngos
DECLARE @parcelName as varchar (50)
SET @parcelName='polis1(OB-OC)'
--/*
INSERT INTO [ListIngos].[dbo].[lists]
           ([n_rec]
           ,[user_id]
           ,[parcel_id]
           ,[reason]
           ,[replace]
           ,[pnum]
           ,[pbnum]
           ,[vsnum]
           ,[fam]
           ,[im]
           ,[ot]
           ,[dr]
           ,[w]
           ,[country]
           ,[rab]
           ,[udldoctp]
           ,[udlser]
           ,[udlnum]
           ,[udldt]
           ,[namevp]
           ,[mr]
           ,[address]
           ,[adressdt]
           ,[regdoctp]
           ,[regdocser]
           ,[regdocnum]
           ,[regdocdt]
           ,[regvp]
           ,[regstop]
           ,[prfam]
           ,[prim]
           ,[prot]
           ,[prdr]
           ,[prdoctp]
           ,[prdocser]
           ,[prdocnum]
           ,[prdocdt]
           ,[prnamevp]
           ,[prvid]
           ,[snils]
           ,[phone]
           ,[dbeg])
           --*/
     select
           newid()
           ,(SELECT [user_id] FROM [ListIngos].[dbo].user_parcels where parcel_name=@parcelName)
           ,(SELECT parcel_id FROM [ListIngos].[dbo].user_parcels where parcel_name=@parcelName)
           ,'����� ��������� ��������(��� ������ ������)'as reason
           ,null as[replace]
           ,sss.enp as pnum
           ,sss.npol as pbnum
           ,null as vsnum
           ,p.fam
           ,p.im
           ,p.ot
           ,convert(date,sss.dr,104)as dr --p.dr
           ,p.w
           ,'������'as country
           ,'����������'as rab
				--,doc_udl
           --,case when doc_udl like('%'+p.docN+'%') udldoctp
           ,case when sss.pid in(989014,681819,622974)then'������� ��' 
				when p.docTp='14'then'������� ��'else'������������� � ��������'end as udldoctp
           --,replace(udlser,'1-','I-')
           ,case when sss.pid in(1147770,681819,798889,622974)then'38 14'when sss.pid in(989014,1161880)then'38 18'
				when sss.pid in(798896,798901,808592)then'38 15'
				when sss.pid in(256857,858101)then'38 17'--when sss.pid=1038256 then'38 15'
				else p.docS end as udlser
           ,case when sss.pid=1147770 then'991710'when sss.pid=989014 then'185606'
				when sss.pid=798896 then'055619'when sss.pid=622974 then'993306'
				when sss.pid=798901 then'047331'when sss.pid=681819 then'989471'
				when sss.pid=798889 then'966845'when sss.pid=1161880 then'201638'
				when sss.pid=256857 then'144972'when sss.pid=858101 then'160199'
				when sss.pid=808592 then'027412'--when sss.pid=858101 then'160199'
				else p.docN end as udlnum
           ,case when sss.pid=1147770 then convert(datetime,'21.01.2015',104)
				when sss.pid=989014 then convert(datetime,'05.07.2018',104)
				when sss.pid=798896 then convert(datetime,'25.12.2015',104)
				--when sss.pid=1038256 then convert(date,'06.05.2015',104)
				when sss.pid=798901 then convert(datetime,'13.10.2015',104)
				when sss.pid=681819 then convert(datetime,'08.11.2014',104)
				when sss.pid=1161880 then convert(datetime,'28.09.2018',104)
				when sss.pid=798889 then convert(datetime,'14.07.2014',104)
				when sss.pid=256857 then convert(datetime,'16.08.2017',104)
				when sss.pid=858101 then convert(datetime,'29.12.2017',104)
				when sss.pid=808592 then convert(datetime,'24.07.2015',104)
				when sss.pid=622974 then convert(datetime,'11.12.2014',104)
				else p.docDt end as udldt
				--622974 '3814 993306 �� 11.12.2014  ����� ����� ������ �� ������� ������� � ��� ���. ������ 460-007' doc_udl
    --       ,case when sss.pid=1147770 then'����� ������ �� ������� ������� � ��� ���. ������'
				--when sss.pid=989014 then'���� ������ �� ������� �������'
				--when sss.pid=798896 then'���������� N1 ����� ������ �� ������� ���. � ��� ���. ������'
				--else p.docN end as namevp
           ,sss.namevp
           ,isNull(p.mr,sss.mr)as mr
           ,sss.adres --as[address]
           ,p.dmj as adressdt
           ,null as regdoctp
           ,null as regdocser
           ,null as regdocnum
           ,null as regdocdt
           ,null as regvp
           ,null as regstop
           ,pr.fam as prfam
           ,pr.im as prim
           ,pr.ot as prot
           ,pr.dr as prdr
           ,case when pr.doctp is not null then'������� ��'else pr.doctp end as prdoctp
           ,pr.docS as prdocser
           ,pr.docN as prdocnum
           ,pr.docDt as prdocdt
           ,pr.docOrg as prnamevp
           ,sss.prvid
           ,replace(replace(sss.ss,' ',''),'-','')as snils
           ,p.phone
           ,(SELECT parcel_date FROM user_parcels where parcel_name=@parcelName)
           from [ListIngos].[dbo].[polis1(OB-OC)] sss
           left outer join srz.dbo.people p on p.id=sss.pid
           left outer join srz.dbo.people pr on sss.pid_pr is not null and pr.id=sss.pid_pr
           where sss.pid not in(762889)
   --        where not exists(select pnum from lists lenp where lenp.pnum=sss.p_num)
			--and sss.pid not in(762889)
			--and not exists(select udlser from lists lud where (lud.udlser+lud.udlnum)=(sss.udlser+sss.udlnum))
			--and not exists(select snils from lists lsn where lsn.snils=sss.snils)


