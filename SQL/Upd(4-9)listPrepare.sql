/*** Upd(4-9)listPrepare ***/
DECLARE @USER AS INT, @DT AS DATE
declare @err varchar(20)
declare @n_recs table(nrec uniqueidentifier)

update LP set err=4 
from lists L
INNER JOIN ListPrepare LP on L.n_rec=LP.n_rec and isNull(LP.kontr,0)=1
LEFT OUTER JOIN srz..PEOPLE sp on lp.pid=sp.ID
where ISNULL(sp.FAM,ISNULL(L.fam,'ERROR'))='ERROR'

update L set fam=sp.FAM 
from lists L
INNER JOIN ListPrepare LP on L.n_rec=LP.n_rec
LEFT OUTER JOIN srz..PEOPLE sp on LP.pid=sp.ID
where sp.FAM is not null and L.fam is null

update LP set err=5 
from lists L
INNER JOIN ListPrepare LP on L.n_rec=LP.n_rec and isNull(LP.kontr,0)=1
LEFT OUTER JOIN srz..PEOPLE sp on lp.pid=sp.ID
where isNull(err,0)=0 and replace(sp.FAM,' ','')<>replace(L.fam,' ','')

update LP set err=6 
from lists L
INNER JOIN ListPrepare LP on L.n_rec=LP.n_rec and isNull(LP.kontr,0)=1
LEFT OUTER JOIN srz..PEOPLE sp on lp.pid=sp.ID
where isNull(err,0)=0 and ISNULL(sp.IM,ISNULL(L.im,'ERROR'))='ERROR'

update L set im=sp.IM 
from lists L
INNER JOIN ListPrepare LP on L.n_rec=LP.n_rec
LEFT OUTER JOIN srz..PEOPLE sp on LP.pid=sp.ID
where sp.IM is not null and L.im is null

update LP set err=7 
from lists L
INNER JOIN ListPrepare LP on L.n_rec=LP.n_rec and isNull(LP.kontr,0)=1
LEFT OUTER JOIN srz..PEOPLE sp on lp.pid=sp.ID
where isNull(err,0)=0 and replace(sp.IM,' ','')<>replace(L.im,' ','')

update LP set err=8 
from lists L
INNER JOIN ListPrepare LP on L.n_rec=LP.n_rec and isNull(LP.kontr,0)=1
LEFT OUTER JOIN srz..PEOPLE sp on lp.pid=sp.ID
where isNull(err,0)=0 and replace(isNull(sp.OT,''),' ','')<>replace(isNull(L.ot,''),' ','')

update LP set err=9 
from lists L
INNER JOIN ListPrepare LP on L.n_rec=LP.n_rec and isNull(LP.kontr,0)=1
LEFT OUTER JOIN srz..PEOPLE sp on lp.pid=sp.ID
where isNull(err,0)=0 and convert(char(8),sp.dr,4)<>convert(char(8),L.dr,4)

update LP set err=10 
from lists L
INNER JOIN ListPrepare LP on L.n_rec=LP.n_rec and isNull(LP.kontr,0)=1
LEFT OUTER JOIN srz..PEOPLE sp on lp.pid=sp.ID
where isNull(err,0)=0 and sp.q not in('46001','46003')

update ListPrepare set kontr=2 where isNull(kontr,0)=1 and isNull(err,0)=0

---select * from listPrepare where err>0

--select count(*)from listPrepare where err>0
select @err='Err='+convert(varchar(10),count(*)) from listPrepare where err>0
print @err
