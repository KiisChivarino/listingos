/*** kontr ListPrepare ***/
declare @dvizit datetime
set @dvizit=CONVERT(datetime,'15.11.2018 11:00',104)
--select u_number as npp,* from users order by u_number
--select top 10 * from dbo.ListPrepare
--select top 10 * from dbo.lists
--select u_number as npp,* from dbo.users order by u_number
select u_number,u_fio
, count(*)as k_vo
,SUM(case when LP.n_rec IS null then 1 else 0 end)as k_N 
,SUM(case when LP.n_rec IS not null then 1 else 0 end)as k_L 
,SUM(case when LP.n_rec IS not null and isNull(LP.err,0)=0 then 0 else 1 end)as k_E 
,SUM(case when LP.n_rec IS not null and isNull(LP.err,0)=0 then 1 else 0 end)as k_V 
,SUM(case when d.n_rec IS not null then 1 else 0 end)as k_d 
,SUM(case when d.n_rec IS not null and d.visit_dvizit>@dvizit then 1 else 0 end)as k_dN 
from dbo.lists L 
left outer join users u on L.[user_id]=u.[user_id]
left outer join ListPrepare LP on LP.n_rec=L.n_rec
left outer join discharge d on d.n_rec=L.n_rec
--left outer join 
group by u_number,u_fio order by u_number,u_fio

