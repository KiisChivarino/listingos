/*** 2018(1)insert_discharge.sql ***/
--CASE WHEN (YEAR(GETDATE())-YEAR(ISNULL(sp.DR,l.dr))+sign(sign((MONTH(GETDATE())-MONTH(ISNULL(sp.DR,l.dr)))*100+DAY(GETDATE())-DAY(ISNULL(sp.DR,l.dr)))+1)-1)<18
insert into discharge
SELECT
l.n_rec																			AS n_rec
,case when sp.q='46001'then'�033'else'�031'end									AS tip_op
,lp.pid																			AS id
,left(ISNULL(sp.FAM,ISNULL(l.fam,'ERROR')),40)											AS person_fam
,left(ISNULL(sp.IM,ISNULL(l.IM,'ERROR')),40)												AS person_im
,left(ISNULL(sp.OT,ISNULL(l.OT,'')),250)													AS person_ot
	--,ISNULL(CONVERT(CHAR(10),sp.DR,104),ISNULL(CONVERT(CHAR(10),l.dr,104),'ERROR')) AS person_dr
,ISNULL(sp.DR,l.dr) AS person_dr
,ISNULL(sp.W,CASE 
				WHEN left(l.W,1)in ('�','�') THEN 1
				WHEN left(l.W,1)in ('�','�') THEN 2
				ELSE(1)END)						AS person_w
,left(ISNULL(sp.MR,ISNULL(l.mr,'ERROR')),100)												AS person_mr
,(CASE 			WHEN l.country='������' THEN 'RUS' 
				WHEN l.country IS NULL THEN ISNULL(sp.CN,'ERR')
				ELSE ISNULL(sp.CN,'LOO') 
			  END)																AS person_c_oksm
,CASE
	WHEN (sp.CN='RUS'or l.country='������') AND l.rab='����������' THEN 1
	WHEN (sp.CN='RUS'or l.country='������')THEN 6
	--WHEN (sp.CN='RUS'or l.country='������') AND l.rab='�� ����������' THEN 6
	ELSE 8 
END																				AS person_kateg
,left(rtrim(ISNULL(l.prfam,'')+' '+ISNULL(l.prim,'')+' '+ISNULL(l.prot,'')),130)			as person_fiopr
,left(ISNULL(CASE WHEN sp.SS='' THEN '' ELSE sp.SS END,CASE 
				WHEN l.snils <>'' THEN SUBSTRING(l.snils,1,3)+'-'+SUBSTRING(l.snils,4,3)+'-'+SUBSTRING(l.snils,7,3)+' '+SUBSTRING(l.snils,10,2) 
				ELSE '' END),14)												AS person_ss

,null,null,null,null,null as oldperson_w

	--,left(ISNULL(sp.DOCTP,CASE 
	--					--WHEN l.udldoctp='������� ��' THEN '14'
	--					WHEN l.udldoctp like'����%' THEN '14'
	--					--WHEN l.udldoctp='������������� � ��������' THEN '3'
	--					WHEN l.udldoctp like'��%' THEN '3'
	--					WHEN l.udldoctp IS NULL THEN isNull(sp.doctp,'ER')
	--					ELSE 'LO'
	--				 END),2)															AS doc_type
	--,left(ISNULL(sp.DOCS,l.udlser),10)														AS doc_ser
	--,left(ISNULL(sp.DOCN,CASE WHEN l.udlnum='' THEN 'ERROR' ELSE l.udlnum END),20)			AS doc_num
	--,ISNULL(CONVERT(CHAR(10),sp.DOCDT,104),CASE WHEN CONVERT(CHAR(10),l.udldt,104)='' THEN 'ERROR' ELSE l.udldt END) AS doc_date
,left(CASE		--WHEN l.udldoctp='������� ��' THEN '14'
		WHEN l.udldoctp like'����%' THEN '14'
					--WHEN l.udldoctp='������������� � ��������' THEN '3'
		WHEN l.udldoctp like'��%' THEN '3'
		ELSE isNull(sp.doctp,'ER')
		END,2)																			AS doc_type
,left(ISNULL(l.udlser,sp.DOCS),10)														AS doc_ser
,left(CASE WHEN isNull(l.udlnum,'')='' THEN ISNULL(sp.DOCN,'ERROR')ELSE l.udlnum END,20)			AS doc_num
,isNull(l.udldt,sp.DOCDT)																AS doc_date


,NULL																					AS doc_exp
,left(CASE WHEN iSnULL(l.namevp,'')='' THEN ISNULL(sp.DOCORG,'ERROR')
					   ELSE l.namevp END,80)											AS doc_name_vp
--,l.regdoctp																	as reg_type
--,case l.regdoctp is null then null else isNull(l.regdocser,'ERROR')end		as reg_ser
--,case l.regdoctp is null then null else isNull(l.regdocnum,'ERROR')end		as reg_num
--,case l.regdoctp is null then null else isNull(l.regdocdt,'ERROR')end			as reg_date
--,case l.regdoctp is null then null else isNull(l.regstop,'ERROR')end			as reg_exp
--,case l.regdoctp is null then null else isNull(l.regstop,'ERROR')end			as reg_name_vp
,null as reg_type,null as reg_ser,null as reg_num,null as reg_date,null as reg_exp,null as reg_name_vp
,null as old_doc_type,null as old_doc_ser,null as old_doc_num,null as old_doc_date,null as old_docexp,null as old_name_vp
,0 as addres_g_bomg
,'38000' as addres_g_subj
,left(isNull(sp.indx,''),6) as addres_g_indx
,isNull(sp.rn,'ERROR') as addres_g_okato
,left(isNull(sp.rnname,'ERROR'),80) as address_g_rnname
,left(isNull(sp.np,isNull(sp.city,' ')),80) as address_g_npname
,left(sp.ul,80) as address_g_ul
,left(sp.dom,7) as address_g_dom
,left(sp.kor,6) as address_g_korp
,left(sp.kv,6)	as address_g_kv
,isNull(sp.dmj,isNull(l.adressdt,isNull(l.udldt,sp.DOCDT)))as address_g_dreg
,left(isNull(sp.rn,'ERROR'),5) as addres_p_subj
,left(isNull(sp.indx,''),6) as addres_p_indx
,left(isNull(sp.rn,'ERROR'),11) as addres_p_okato
,left(isNull(sp.rnname,'ERROR'),80) as address_p_rnname
,left(isNull(sp.np,isNull(sp.city,' ')),80) as address_p_npname
,left(sp.ul,80) as address_p_ul
,left(sp.dom,7) as address_p_dom
,left(sp.kor,6) as address_p_korp
,left(sp.kv,6)	as address_p_kv
--,getdate()as vist
--,isNull(sp.dmj,isNull(l.adressdt,l.udldt))as address_p_dreg
,getdate() as visit_dvizit
,(case when l.prfam is null then'1'else'2'end)as visit_method
,'0'as visit_petition
--,4 as visit_rsmo
,case when sp.q='46001'then 4 else 2 end as visit_rsmo
,0 as visit_rpolis
,(case when sp.npol like'02%'then 2 else 1 end)as visit_fpolis
,'38000'as insurance_ter_st,sp.enp as insurance_enp,'1045207042528'as insurance_ogrnsmo,0 as insurance_erp
,3 as polis_vpolis,sp.npol as polis_npolis,null as polis_spolis
,getdate() as polis_dbeg,null as polis_dend,null as polis_dstop

FROM
lists l
INNER JOIN listPrepare lp on l.n_rec=lp.n_rec
LEFT OUTER JOIN srz..PEOPLE sp on lp.pid=sp.ID
where isNull(lp.kontr,0)=2 and isNull(lp.err,0)=0
and not exists(select * from discharge d where d.n_rec=l.n_rec)
and not(sp.DR is null and l.dr is null)
and not(sp.W is null and l.W is null)
--and not exists(select * from discharge d where d.person_ss=l.snils)
--and not(left(ISNULL(CASE WHEN sp.SS='' THEN NULL ELSE sp.SS END,CASE 
--				WHEN l.snils <>'' THEN SUBSTRING(l.snils,1,3)+'-'+SUBSTRING(l.snils,4,3)+'-'+SUBSTRING(l.snils,7,3)+' '+SUBSTRING(l.snils,10,2) 
--				ELSE 'ERROR' END),14)='ERROR')
/*select 
sp.ENP,sp.RNNAME,sp.CITY,sp.NP,sp.ul,sp.dom,sp.KOR,sp.kv,
l.address,
CASE
--WHEN l.address like ('%'+sp.UL+'%') then 1--and l.address like ('%'+sp.np+'%') THEN 1
WHEN l.address like ('%'+ISNULL(STRING_SPLIT(SP.CITY,' '),sp.np)+'%') THEN 1
ELSE 0
END
from lists l
left outer join listPrepare lp on lp.n_rec=l.n_rec
left outer join srz..people sp on lp.pid=sp.ID*/

--SELECT * FROM
--lists l
--INNER JOIN listPrepare lp on l.n_rec=lp.n_rec
--LEFT OUTER JOIN srz..PEOPLE sp on lp.pid=sp.ID