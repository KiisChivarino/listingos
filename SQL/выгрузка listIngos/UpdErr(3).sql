/*** UpdErr(3) ***/
/*** 2018_ListIngos_update_snils_enp_udl.sql ***/
/*4-9
--UPDATE l SET dr=convert(date,'26.07.1962',104) /*err=9
--*/
--UPDATE l SET fam=sp.fam /*err=5
--*/
--UPDATE l SET im=sp.im /*err=7
--*/
--UPDATE l SET ot=isNull(sp.ot,'') /*err=8
--*/
--UPDATE l SET dr=sp.dr /*err=9
--*/
--UPDATE l SET fam=sp.fam,im=sp.im,ot=isNull(sp.ot,'',dr=sp.dr /*err=5-9
--*/
--UPDATE l SET fam=sp.fam /*err=5
select lp.kontr,lp.err,lp.pid
	,lp.pid_enp,lp.pid_snils,lp.pid_udl
	,sp.q
	--,l.fam,l.im,l.ot,l.dr
	,(l.fam+' '+l.im+' '+isNull(l.ot,'')+' '+convert(char(10),l.dr,104))as fiodr
	,(sp.fam+' '+sp.im+' '+isNull(sp.ot,'')+' '+convert(char(10),sp.dr,104))as fiodr1
	,l.n_rec
--,l.*
--*/
from lists l
LEFT OUTER JOIN listPrepare lp on l.n_rec=lp.n_rec
LEFT OUTER JOIN srz..people sp on lp.pid=sp.ID
--WHERE lp.err>0 --and sp.q not in('46001','46003')
WHERE lp.err between 1 and 9 --and sp.q not in('46001','46003')
--WHERE lp.err between 5 and 9 --and sp.q not in('46001','46003')
	--and lp.pid not in(381796,655762,748466,1473638,261023,837762,1390695,207945,1101392,878372)
	--and lp.pid in(572675,674228,200677,824039,1065523,1291787,1497032,244438,489778,763928,1175476,1272816)
	--and replace(l.fam+l.im+isNull(l.ot,'')+convert(char(10),l.dr,104),' ','')=replace(sp.fam+sp.im+isNull(sp.ot,'')+convert(char(10),sp.dr,104),' ','')
	--and lp.err=5 and replace(l.im+isNull(l.ot,'')+convert(char(10),l.dr,104),' ','')=replace(sp.im+isNull(sp.ot,'')+convert(char(10),sp.dr,104),' ','')
	and lp.err=7 and replace(l.fam+isNull(l.ot,'')+convert(char(10),l.dr,104),' ','')=replace(sp.fam+isNull(sp.ot,'')+convert(char(10),sp.dr,104),' ','')
	--and lp.err=8 and replace(l.fam+l.im+convert(char(10),l.dr,104),' ','')=replace(sp.fam+sp.im+convert(char(10),sp.dr,104),' ','')
	--and lp.err=9 and replace(l.fam+l.im+isNull(l.ot,''),' ','')=replace(sp.fam+sp.im+isNull(sp.ot,''),' ','')
--and lp.pid=1249229
-- UPDATE ListPrepare SET kontr=1,err=0 where kontr=1 and err=9
-- UPDATE ListPrepare SET kontr=1,err=0 where kontr=1 and err between 4 and 9
-- UPDATE ListPrepare SET kontr=1,err=0 where pid=1249229 and kontr=1 and err=9
-- UPDATE ListPrepare SET kontr=1,err=0 where pid=1357811 and kontr=0 and err=1
--*/
--1	5	207945	207945	NULL	207945	46001	Калугина Александра Сергеевна 07.03.2007	КАЛУГИН АЛЕКСАНДР СЕРГЕЕВИЧ 07.03.2007	B4E825AE-114A-4B8E-B80D-AA4F03263522

 -- UPDATE ListPrepare SET  pid=null,pid_enp=null,pid_snils=null,pid_udl=null,kontr=0,err=0 where err>0

/*испр.СНИЛС=УДЛ
update lists set pnum='4676050890000334',pbnum='01036916022'  where n_rec='143C108A-B8F1-40D9-9F05-B88E20A082D9'
update listPrepare set pid_enp=pid,err=0,kontr=0 where n_rec='143C108A-B8F1-40D9-9F05-B88E20A082D9'
--*/
/*испр.СНИЛС
update listPrepare set pid=pid_enp, pid_snils=pid_enp,err=0,kontr=0 where n_rec='2B6680A2-E0E8-4E1E-BEC2-501D8167E2E8'
--*/

/*испр.СНИЛС
update lists set snils=''  where n_rec='D49FAFE7-C126-407A-A10D-0B070F520967'
update listPrepare set pid_snils=null,err=0,kontr=0 where n_rec='D49FAFE7-C126-407A-A10D-0B070F520967'
--*/
/*контр.СНИЛС
--UPDATE l SET snils=replace(replace(isNull(sp.ss,''),' ',''),'-','') /*
--*/
--UPDATE l SET pnum=sp2.enp,pbnum=sp2.npol /*
select lp.pid,lp.pid_enp,lp.pid_snils,lp.pid_udl,l.snils--,sp.ss
	,replace(replace(isNull(sp.ss,''),' ',''),'-','')as sss
	--,sp1.enp as enp1,sp2.enp as enp2--,l2.snils as snils2
	--,sp3.enp as enp3
	,l.pnum as enp,l.pbnum as blank
	--,l.fam,l.im,l.ot,convert(char(10),l.dr,104)as sDR
	,(l.fam+' '+l.im+' '+l.ot+' '+convert(char(10),l.dr,104))as fiodr
	,(sp.fam+' '+sp.im+' '+sp.ot+' '+convert(char(10),sp.dr,104))as fiodr1
	,(sp2.fam+' '+sp2.im+' '+sp2.ot+' '+convert(char(10),sp2.dr,104))as fiodr2
	,l.n_rec

	--,replace(l.fam+l.im+l.ot+convert(char(10),l.dr,104),' ','')as fiodr1
	--,replace(sp.fam+sp.im+sp.ot+convert(char(10),sp.dr,104),' ','')as fiodr2
--*/ 
from lists l
LEFT OUTER JOIN listPrepare lp on l.n_rec=lp.n_rec
LEFT OUTER JOIN srz..people sp on sp.ID=isNull(lp.pid,lp.pid_enp)
LEFT OUTER JOIN srz..people sp1 on sp1.ID=lp.pid_enp
LEFT OUTER JOIN srz..people sp2 on sp2.ID=lp.pid_snils
--LEFT OUTER JOIN srz..people sp3 on sp3.ID=lp.pid_udl
--LEFT OUTER JOIN lists l2 on l2.pnum=sp2.enp
WHERE lp.err in(1)
	--and lp.pid_snils=lp.pid_udl
	--and lp.pid=lp.pid_udl
	--and lp.pid=lp.pid_snils
	--and l.fam=sp.fam and l.im=sp.im and l.ot=sp.ot and convert(char(10),l.dr,104)=convert(char(10),sp.dr,104)
	--and replace(l.fam+l.im+l.ot+convert(char(10),l.dr,104),' ','')=replace(sp.fam+sp.im+sp.ot+convert(char(10),sp.dr,104),' ','')
	--and replace(l.fam+l.im+l.ot+convert(char(10),l.dr,104),' ','')=replace(sp2.fam+sp2.im+sp2.ot+convert(char(10),sp2.dr,104),' ','')
	--and l.snils=l2.snils
	--and ped_enp in(245651)
--WHERE lp.err in(1,3)
--and lp.pid=1357811
--and sp.ss is null
--and lp.pid is not null
--*/
-- UPDATE ListPrepare SET kontr=0,err=0 where pid=1357811 and kontr=0 and err=1
-- UPDATE ListPrepare SET kontr=1,err=0 where pid=1357811 and kontr=0 and err=1
-- UPDATE ListPrepare SET pid_snils=pid,kontr=1,err=0 where pid=671507 and kontr=0 and err=1

--update ListPrepare set err=0,kontr=0,pid_snils=null where isNull(err,0)in(1)and isNull(pid,0)=isNull(pid_udl,0)
--update ListPrepare set err=0,kontr=0,pid_snils=null where isNull(err,0)in(1)and pid=pid_udl
--update ListPrepare set err=0,kontr=0,pid_enp=null where isNull(err,0)in(1)

/*
update ListPrepare set err=0,kontr=0,pid_snils=null where isNull(err,0)in(1,3)

UPDATE lp SET lp.pid_snils=ps.ID
FROM [ListIngos].[dbo].[ListPrepare] lp
INNER JOIN [ListIngos].[dbo].Lists l ON l.n_rec=lp.n_rec
LEFT OUTER JOIN srz..people ps ON ps.SS=(SUBSTRING(l.snils,1,3)+'-'+SUBSTRING(l.snils,4,3)+'-'+SUBSTRING(l.snils,7,3)+' '+SUBSTRING(l.snils,10,2))
WHERE lp.pid_snils is null and isNull(kontr,0)=0 and isNull(err,0)=0
--*/
/*
UPDATE lp SET lp.pid_enp=pe.ID
FROM [ListIngos].[dbo].[ListPrepare] lp
INNER JOIN [ListIngos].[dbo].Lists l ON l.n_rec=lp.n_rec
left outer join srz..people  pe on l.pnum=pe.enp
WHERE lp.pid_enp is null and isNull(kontr,0)=0 and isNull(err,0)=0
--*/
/*
UPDATE lp SET lp.pid_udl=pd.ID
FROM [ListIngos].[dbo].[ListPrepare] lp
INNER JOIN [ListIngos].[dbo].Lists l ON l.n_rec=lp.n_rec
left outer join srz..people pd on 
(CASE 
	 WHEN udldoctp='Паспорт РФ' THEN STUFF(REPLACE(udlser,' ',''),3,0,' ')
	 ELSE udlser
END+udlnum)=pd.DOCS+pd.DOCN
WHERE lp.pid_udl is null and isNull(kontr,0)=0 and isNull(err,0)=0
--*/
/*
UPDATE ListPrepare SET pid=
CASE 
WHEN (pid_enp IS NOT NULL AND pid_snils IS NOT NULL AND pid_enp=pid_snils) THEN pid_enp
WHEN (pid_udl IS NOT NULL AND pid_enp IS NOT NULL AND pid_enp=pid_udl) THEN pid_udl
WHEN (pid_snils IS NOT NULL AND pid_udl IS NOT NULL AND pid_snils=pid_udl) THEN pid_snils
ELSE NULL
END
where isNull(kontr,0)=0 and isNull(err,0)=0

update ListPrepare set err=1 where pid_enp IS NOT NULL AND pid_snils IS NOT NULL AND pid_enp<>pid_snils and isNull(kontr,0)=0

update ListPrepare set err=2 where pid_udl IS NOT NULL AND pid_enp IS NOT NULL AND pid_enp<>pid_udl and isNull(err,0)=0 and isNull(kontr,0)=0

update ListPrepare set err=3 where pid_snils IS NOT NULL AND pid_udl IS NOT NULL AND pid_snils<>pid_udl and isNull(err,0)=0 and isNull(kontr,0)=0

update ListPrepare set pid=isNull(pid_enp,isNull(pid_snils,isNull(pid_udl,0)))where isNull(err,0)=0 and isNull(err,0)=0 and isNull(kontr,0)=0

update ListPrepare set kontr=1 where isNull(pid,0)>0 and isNull(err,0)=0 and isNull(kontr,0)=0
--*/

--select count(*)as err from listPrepare where err in(1,3)

--select * from listPrepare where err>0

/*возможно не записаны из исходного файла
4656230835000201	01059957848		исправленная запись n_rec='420C4576-1904-42B2-98CF-037D59576C6B' 4649420873000094 01037438309
4671150879000191	01044536336		исправленная запись n_rec='96CD89CB-ACF1-4315-BDC3-3B03A1ACFF18' 4675750834000099 01044536337
4694989742000026	01035887534		исправленная запись n_rec='F2D46F28-D6C5-4E32-9B6C-82BF2652B60D' 4656710826000039 02019730525
4690199793000115	01087770829		исправленная запись n_rec='52E5DE0B-9267-4029-8B20-C9F33EA523BA' 4657310896000206 01044536164
3653510877000184	01078697357		исправленная запись n_rec='A7F2BF46-88C7-4CED-925D-ECC342BED716' 5757510884000177 01098551545
4658840875000057	NULL
4654900895000182	NULL
4655730883000293	NULL
4650140888000071	NULL
4655710870000035	NULL
4655030889000235	NULL
4655600890000182	NULL
4658420875000157	NULL
4655720875000203	NULL
4047900886000338	NULL
4658200828000136	01034851373
4649010838000162	NULL
4658630883000037	NULL
--		исправленная запись n_rec='' */