/*** polis1(OB-OC) ***/
--update [ListIngos].[dbo].[polis1(OB-OC)] set npol='02018313337',enp='4653410886000119',ss='149-137-990 97'where ss='149-137-990-97'
--update [ListIngos].[dbo].[polis1(OB-OC)] set ss='045-738-094 77'where ss='045-738 094 77'
--update sss set pid=ps.id,enp=ps.enp,npol=ps.npol/*
--update sss set pid=pf.id,enp=pf.enp,npol=pf.npol/*
select 
	sss.pid as pid1
	,sss.npol as npol1
	,sss.ss as ss1
	--,ps.id
	--,pf.id as idF
	,pn.id as idN
	--,ps.q,ps.dbeg
	--,pf.q as qF,pf.ss as ssF
	,pn.q as qF,pn.ss as ssF
	,sss.ss as ssI
	--,lower(replace(pn.fam+pn.fam+pn.ot,' ',''))+convert(char(10),pn.dr,104)as fiodrN
	--,lower(replace(sss.fio,' ',''))+sss.dr as fiodrI
	,sss.* 
	--*/
from [ListIngos].[dbo].[polis1(OB-OC)] sss
--left outer join srz..people pf on pf.ss=sss.ss 
--left outer join srz..people ps on replace(ps.ss,' ','')=replace(sss.ss,' ','') 
--left outer join srz..people pf on lower(replace(pf.fam+pf.im+pf.ot,' ',''))+convert(char(10),pf.dr,104)=lower(replace(sss.fio,' ',''))+sss.dr 
--left outer join srz..people pn on pn.enp=sss.enp
left outer join srz.dbo.people pn on pn.enp=sss.enp
--left outer join sql_l193.srz.dbo.people pn on pn.enp=sss.enp
--where pn.q='46001'
--where isNull(sss.pid,0)=0
--	and ps.id is not null