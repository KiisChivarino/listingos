use ListIngos
DECLARE @parcelName as varchar (50)
SET @parcelName='list1_03_181023'

--IF (SELECT count(*) FROM list_02_181023 ul 
--inner join lists lsn on lsn.snils=ul.snils
--inner join lists lud on (lud.udlser+lud.udlnum)=(ul.udlser+ul.udlnum)
--inner join lists lenp on lenp.pnum=ul.pnum)>0
--BEGIN
--SELECT lsn.snils,lud.udlser,lud.udlnum,lenp.pnum, ul.fam,ul.im,ul.ot,ul.dr FROM list_02_181023 ul 
--inner join lists lsn on lsn.snils=ul.snils
--inner join lists lud on (lud.udlser+lud.udlnum)=(ul.udlser+ul.udlnum)
--inner join lists lenp on lenp.pnum=ul.pnum
----select snils,COUNT(*)as k_vo from lists group by snils)ss where k_vo>1
--END
--ELSE
--BEGIN
/*
INSERT INTO [ListIngos].[dbo].[lists]
           ([n_rec]
           ,[user_id]
           ,[parcel_id]
           ,[reason]
           ,[replace]
           ,[pnum]
           ,[pbnum]
           ,[vsnum]
           ,[fam]
           ,[im]
           ,[ot]
           ,[dr]
           ,[w]
           ,[country]
           ,[rab]
           ,[udldoctp]
           ,[udlser]
           ,[udlnum]
           ,[udldt]
           ,[namevp]
           ,[mr]
           ,[address]
           ,[adressdt]
           ,[regdoctp]
           ,[regdocser]
           ,[regdocnum]
           ,[regdocdt]
           ,[regvp]
           ,[regstop]
           ,[prfam]
           ,[prim]
           ,[prot]
           ,[prdr]
           ,[prdoctp]
           ,[prdocser]
           ,[prdocnum]
           ,[prdocdt]
           ,[prnamevp]
           ,[prvid]
           ,[snils]
           ,[phone]
           ,[dbeg])
     --*/
     select
           newid()
           ,(SELECT [user_id] FROM [ListIngos].[dbo].user_parcels where parcel_name=@parcelName)
           ,(SELECT parcel_id FROM [ListIngos].[dbo].user_parcels where parcel_name=@parcelName)
           ,reason
           ,replace
           ,pnum
           ,pbnum
           ,vsnum
           ,fam
           ,im
           ,ot
           ,dr
           ,w
           ,country
           ,rab
           ,udldoctp
           ,replace(udlser,'1-','I-')
           ,udlnum
           ,udldt
           ,namevp
           ,mr
           ,address
           ,adressdt
           ,regdoctp
           ,regdocser
           ,regdocnum
           ,regdocdt
           ,regvp
           ,regstop
           ,prfam
           ,prim
           ,prot
           ,prdr
           ,prdoctp
           ,prdocser
           ,prdocnum
           ,prdocdt
           ,prnamevp
           ,prvid
           ,snils
           ,phone
           ,(SELECT parcel_date FROM user_parcels where parcel_name=@parcelName)
           from [ListIngos].[dbo].[list1_03_181023] sss
           where not exists(select pnum from lists lenp where lenp.pnum=sss.pnum)
			--and not exists(select udlser from lists lud where (lud.udlser+lud.udlnum)=(sss.udlser+sss.udlnum))
			--and not exists(select snils from lists lsn where lsn.snils=sss.snils)
--END


