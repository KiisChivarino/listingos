/*** �������� ������� �� �� ������-����� ***/
USE ListIngos
--DECLARE @polis VARCHAR(20) SELECT @polis=(SELECT '161528854')
DECLARE @policeTable TABLE(id_num INT IDENTITY(1,1) PRIMARY KEY, polis VARCHAR(20) NOT NULL);	-- ��������� ������� �������
DECLARE @notTable TABLE(id_num INT IDENTITY(1,1) PRIMARY KEY, polis VARCHAR(20) NOT NULL);		-- ��������� ����������� ������� �������
DECLARE 
@dstopdef DATE,																					--��� ������� ���� ��������� ����� �������� ������ ����������� (��������� ���� ����)
@TIP_OP CHAR(4),																				--��� ��������������� ������� ���� ��������
@beg DATETIME, @end DATETIME,																	--��� ������� ������� �� ����
@RUSlast CHAR(3),																				--��������� ����� ������� ��� �������
@INlast CHAR(3),																				--��������� ����� ������� ��� �����������
@nat INT,
@prz INT,
@list BIT,

--��� ����������� DEND
@dend DATETIME,
@currentDate DATETIME,
@weekday INT,
@numdays INT
;

--DECLARE @beg DATE
--DECLARE @end date
SET @beg =  CONVERT(DATETIME,'01.11.2018 11:00:00',104)
SET @end =  CONVERT(DATETIME,'12.11.2018 10:35:00',104)
select d.insurance_enp,sp.q,u.prz,person_fam,person_im,person_ot,person_dr,isnull(addres_g_rnname,'')+' '+isnull(addres_g_npname,'')+' '+isnull(addres_g_ul,'')+' '+isnull(addres_g_dom,'')+' '+isnull(addres_g_korp,'')+' '+isnull(addres_g_kv,'') as adress, l.phone
FROM dbo.discharge d(NOLOCK)															
left outer join srz..OKATO so on so.CODE=d.addres_g_okato
left outer join srz..OKATO ro on ro.CODE=left(d.addres_g_okato,5)+'000000'
--left outer join srz..people sp on sp.q='46001' and sp.ID=d.id and sp.DSTOP is null
left outer join srz..people sp on sp.ID=d.id and sp.DSTOP is null
left outer join lists l on l.n_rec=d.n_rec
left outer join users u on l.user_id=u.user_id
where 
Q<>'46001'
and Q<>'46003'
and visit_dvizit between @beg and @end