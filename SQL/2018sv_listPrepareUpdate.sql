/*** listPrepareUpdate ***/
DECLARE @USER AS INT, @DT AS DATE
declare @n_recs table(nrec uniqueidentifier)

SET @USER=1
SET @DT=CONVERT(datetime,'18.10.2018',104)

INSERT INTO @n_recs 
SELECT n_rec from lists l
LEFT OUTER JOIN users u ON l.user_id=u.user_id
where
dt_discharge is null
AND u.u_number=@USER
AND dbeg=@dt

INSERT INTO ListPrepare (n_rec) SELECT nrec FROM @n_recs new
left outer join listPrepare old on old.n_rec=new.nrec
where old.n_rec is null

UPDATE lp SET lp.pid_snils=ps.ID
FROM [ListIngos].[dbo].[ListPrepare] lp
INNER JOIN [ListIngos].[dbo].Lists l ON l.n_rec=lp.n_rec
LEFT OUTER JOIN srz..people ps ON ps.SS=(SUBSTRING(l.snils,1,3)+'-'+SUBSTRING(l.snils,4,3)+'-'+SUBSTRING(l.snils,7,3)+' '+SUBSTRING(l.snils,10,2))
WHERE lp.pid_snils is null

UPDATE lp SET lp.pid_enp=pe.ID
FROM [ListIngos].[dbo].[ListPrepare] lp
INNER JOIN [ListIngos].[dbo].Lists l ON l.n_rec=lp.n_rec
left outer join srz..people  pe on l.pnum=pe.enp
WHERE lp.pid_enp is null

UPDATE lp SET lp.pid_udl=pd.ID
FROM [ListIngos].[dbo].[ListPrepare] lp
INNER JOIN [ListIngos].[dbo].Lists l ON l.n_rec=lp.n_rec
left outer join srz..people pd on 
(CASE 
	 WHEN udldoctp='Паспорт РФ' THEN STUFF(REPLACE(udlser,' ',''),3,0,' ')
	 ELSE udlser
END+udlnum)=pd.DOCS+pd.DOCN
WHERE lp.pid_udl is null

UPDATE ListPrepare SET pid=
CASE 
WHEN (pid_enp IS NOT NULL AND pid_snils IS NOT NULL AND pid_enp=pid_snils) THEN pid_enp
WHEN (pid_udl IS NOT NULL AND pid_enp IS NOT NULL AND pid_enp=pid_udl) THEN pid_udl
WHEN (pid_snils IS NOT NULL AND pid_udl IS NOT NULL AND pid_snils=pid_udl) THEN pid_snils
ELSE NULL
END

update ListPrepare set err=1 where not(pid_enp IS NOT NULL AND pid_snils IS NOT NULL AND pid_enp=pid_snils)

update ListPrepare set err=2 where not(pid_udl IS NOT NULL AND pid_enp IS NOT NULL AND pid_enp=pid_udl)

update ListPrepare set err=3 where not(pid_snils IS NOT NULL AND pid_udl IS NOT NULL AND pid_snils=pid_udl)

update ListPrepare set pid=isNull(pid_enp,isNull(pid_snils,isNull(pid_udl,0)))where isNull(err,0)=0

