-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION largestStr 
(
	-- Add the parameters for the function here
	@inputStr varchar(255),
	@delimiter char(1)
)
RETURNS varchar(255)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @bigStr varchar(255)

	-- Add the T-SQL statements to compute the return value here
	--SELECT <@ResultVar, sysname, @Result> = <@Param1, sysname, @p1>
	
set @bigStr=(select value from
	(select value, LEN(value) as valueLen
		from [dbo].[fn_Split](@inputStr,@delimiter)
	)vl
	where valueLen=(
		select MAX(valueLen) from (select *, LEN(value) as valueLen
			from [dbo].[fn_Split](@inputStr,@delimiter)
		)v2))

	-- Return the result of the function
	RETURN @bigStr

END
GO

