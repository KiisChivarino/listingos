/*** �������� ������� �� �� ������-����� ***/
USE ListIngos
--DECLARE @polis VARCHAR(20) SELECT @polis=(SELECT '161528854')
DECLARE @policeTable TABLE(id_num INT IDENTITY(1,1) PRIMARY KEY, polis VARCHAR(20) NOT NULL);	-- ��������� ������� �������
DECLARE @notTable TABLE(id_num INT IDENTITY(1,1) PRIMARY KEY, polis VARCHAR(20) NOT NULL);		-- ��������� ����������� ������� �������
DECLARE 
@dstopdef DATE,																					--��� ������� ���� ��������� ����� �������� ������ ����������� (��������� ���� ����)
@TIP_OP CHAR(4),																				--��� ��������������� ������� ���� ��������
@beg DATETIME, @end DATETIME,																	--��� ������� ������� �� ����
@RUSlast CHAR(3),																				--��������� ����� ������� ��� �������
@INlast CHAR(3),																				--��������� ����� ������� ��� �����������
@nat INT,
@prz INT,
@list BIT,

--��� ����������� DEND
@dend DATETIME,
@currentDate DATETIME,
@weekday INT,
@numdays INT
;

--DECLARE @beg DATE
--DECLARE @end date
SET @beg =  CONVERT(DATETIME,'13.11.2018 11:00:00',104)
SET @end =  CONVERT(DATETIME,'19.11.2018 10:35:00',104)
--SELECT @nat= $(cmdnat) --0 ������� 1 ����������
--SELECT @prz = $(cmdprz)--*/

/*SELECT @nat= 1--$(cmdnat) --0 ������� 1 ����������
SELECT @prz = 10--$(cmdprz)--*/


/*SELECT @dstopdef = CONVERT(DATE,(SELECT CASE WHEN MONTH(GETDATE())=12 THEN CONVERT(CHAR(4),YEAR(GETDATE())+1) ELSE CONVERT(CHAR(4),YEAR(GETDATE())) END+'-12-31'));
--����������� ���� DEND
SELECT @numdays = 0;
SELECT @currentDate = DATEADD(DAY, -1, GETDATE())
WHILE @numdays<30
BEGIN
	SELECT @currentDate=DATEADD(DAY, 1, @currentDate)
	SELECT @weekday = DATEPART (WEEKDAY, @currentDate)
	IF(
		@weekday=5 OR @weekday=6
		OR (MONTH(@currentDate)=1 AND DAY(@currentDate) BETWEEN 1 AND 8)
		OR (MONTH(@currentDate)=2 AND DAY(@currentDate)=23)
		OR (MONTH(@currentDate)=3 AND DAY(@currentDate)=8)
		OR (MONTH(@currentDate)=3 AND DAY(@currentDate)=9)
		OR (MONTH(@currentDate)=4 AND DAY(@currentDate)=30)
		OR (MONTH(@currentDate)=5 AND DAY(@currentDate)=1)
		OR (MONTH(@currentDate)=5 AND DAY(@currentDate)=2)
		OR (MONTH(@currentDate)=5 AND DAY(@currentDate)=9)
		OR (MONTH(@currentDate)=6 AND DAY(@currentDate)=11)
		OR (MONTH(@currentDate)=6 AND DAY(@currentDate)=12)
		OR (MONTH(@currentDate)=11 AND DAY(@currentDate)=4)
		OR (MONTH(@currentDate)=11 AND DAY(@currentDate)=5)
	) CONTINUE
	ELSE(
		SELECT @numDays = @numDays+1
	)
END
SELECT @dend = @currentDate
--����������� ���� DEND ����������

SELECT @list=0

--��������� �������� ����������: 
--@RUSlast - ��������� ����� ������� �������, 
--@INlast - ��������� ����� ������� �����������, 
--@TIP_OP - ��� ������� � '�040' ��� '�060' ('' - �� ���������), 
--@beg,@end - ������ ��� ������ �������

SET @RUSlast='300' 
SET @INlast='741'
SET @TIP_OP=''*/

--������������ ������ �������--
--SELECT @list=1 INSERT INTO @policeTable (polis) VALUES ('000000000')
--/*--close discharge
/*
SELECT @beg=CONVERT(DATETIME,'00.00.0000',104),@end=CONVERT(DATETIME,'00.00.0000 23:59:59',104) 

INSERT INTO @policeTable (polis)
	SELECT	DISTINCT polis_number FROM dbo.discharge p
	WHERE
		visit_dvizit BETWEEN @beg AND @end
*/		

--������������ ����� XML--

/*SELECT DISTINCT

'<?xml version="1.0" encoding="Windows-1251"?>'+
'<OPLIST '+
	'VERS="2.1a" '+ 
	'FILENAME="i46003_'+u.prz+'_'+ 
		CASE 
			WHEN LEN(CONVERT(CHAR(2),MONTH(GETDATE())))=1 THEN '0'+CONVERT(VARCHAR(2),MONTH(GETDATE()))
			WHEN LEN(CONVERT(CHAR(2),MONTH(GETDATE())))=2 THEN CONVERT(CHAR(2),MONTH(GETDATE()))
			ELSE ''
		END+SUBSTRING(CONVERT(CHAR(4),YEAR(GETDATE())),3,2)+
		CASE
			WHEN nat='0' THEN 
				CASE 
					WHEN LEN(CONVERT(VARCHAR(3),CONVERT(INT, @RUSlast)+row))=3 THEN CONVERT(VARCHAR(3),CONVERT(INT, @RUSlast)+row)
					WHEN LEN(CONVERT(VARCHAR(3),CONVERT(INT, @RUSlast)+row))=2 THEN '0'+CONVERT(VARCHAR(3),CONVERT(INT, @RUSlast)+row)
					WHEN LEN(CONVERT(VARCHAR(3),CONVERT(INT, @RUSlast)+row))=1 THEN '00'+CONVERT(VARCHAR(3),CONVERT(INT, @RUSlast)+row)
					ELSE ''
				END
			WHEN nat='1' THEN 
				CONVERT(VARCHAR(3),CONVERT(INT, @INlast)+row)
			ELSE ''
		END+
		'.xml'+
	'" '+
	'SMOCOD="46003" '+
	'PRZCOD="'+u.prz+'" '+
	'NRECORDS="'+CONVERT(VARCHAR(4),cnt)+'" '+
'>' 
FROM (
	SELECT DISTINCT 
		ROW_NUMBER() OVER(PARTITION BY (CASE WHEN assured_nationality<>'rus' THEN '1' ELSE '0' END) ORDER BY CASE WHEN @TIP_OP='' THEN s.code_structure ELSE '019' END ASC) AS row,
		CASE WHEN @TIP_OP='' THEN s.code_structure ELSE '019' END AS prz, 
		COUNT(*)as cnt, 
		CASE WHEN assured_nationality<>'rus' THEN '1' ELSE '0' END AS nat 
	FROM [Global_v2_46_test].[dbo].[risIF_Assured] p
	LEFT OUTER JOIN risIF_Polises n (NOLOCK)ON n.id_assured=p.id_assured
	LEFT OUTER JOIN risIF_AssuredStatments z (NOLOCK)ON z.id_assured=p.id_assured 
	LEFT OUTER JOIN risSC_Structures s(NOLOCK)ON s.id_structure=n.id_structure
	WHERE
		--n.id_polis_status IN('821DC9FE-756D-4D9A-AF01-A272B6B95B7D','3A4356EE-D8F3-45F4-92F6-D5DE743DD8A9')
		
			(CASE 
				WHEN (@list = 1 OR @TIP_OP='�060') 
					THEN CASE WHEN ((n.polis_number in(SELECT polis FROM @policeTable))or(replace(n.polis_blank_serial+n.polis_blank_number,' ','')in(SELECT polis FROM @policeTable))) THEN 1 ELSE 0 END
				ELSE
					CASE WHEN z.date_visit BETWEEN  @beg AND @end 
							  and n.id_polis_status IN('821DC9FE-756D-4D9A-AF01-A272B6B95B7D','3A4356EE-D8F3-45F4-92F6-D5DE743DD8A9')
					THEN 1 ELSE 0 END 
			END)=1
		AND z.date_visit = (SELECT MAX(date_visit) FROM risIF_AssuredStatments WHERE id_assured=n.id_assured)
	GROUP BY 
		CASE WHEN @TIP_OP='' THEN s.code_structure ELSE '019' END,
		CASE WHEN assured_nationality<>'rus' THEN '1' ELSE '0'END
	--ORDER BY code_structure
	)pack
	WHERE CONVERT(INT,nat)=@nat AND CONVERT(INT,prz)=(SELECT CASE WHEN @TIP_OP='�040' OR @TIP_OP='�060' THEN 19 ELSE @prz END)
*/

--������������ ������� ��������������

SELECT 
	'<OP '+
		'N_REC="'+CASE WHEN ISNULL(CAST (d.n_rec as varchar(36)),'')<>'' THEN REPLACE(CAST (d.n_rec as varchar(36)),' ','') ELSE 'ERROR' END+'" '+
		'TIP_OP="'+CASE WHEN ISNULL(tip_op,'')<>'' THEN tip_op ELSE 'ERROR' END+'" '+
		CASE WHEN ISNULL(CONVERT(VARCHAR(10),d.id),'')<>'' THEN
		'ID="'+REPLACE(CONVERT(VARCHAR(10),d.id),' ','')+'" '
		ELSE '' END+
	' >'+
		'<PERSON '+
			'FAM="'+CASE WHEN ISNULL(person_fam,'')<>'' THEN REPLACE(person_fam,' ','') ELSE 'ERROR' END+'" '+
			'IM="'+CASE WHEN ISNULL(person_im,'')<>'' THEN REPLACE(person_im,' ','') ELSE 'ERROR' END+'" '+
			CASE WHEN ISNULL(REPLACE(person_ot,' ',''),'')<>'' THEN
			'OT="'+REPLACE(ISNULL(person_ot,''),' ','')+'" '
			ELSE '' END+
			'DR="'+CASE WHEN ISNULL(person_dr,'')<>'' THEN REPLACE(CONVERT(CHAR(10),person_dr,20),' ','') ELSE 'ERROR' END+'" '+
			'W="'+CASE WHEN ISNULL(person_w,0)<>0 THEN REPLACE(CONVERT(CHAR(1),person_w),' ','') ELSE 'ERROR' END+'" '+
			'MR="'+CASE WHEN ISNULL(person_mr,'')<>'' THEN person_mr ELSE 'ERROR' END+'" '+
			'C_OKSM="'+CASE WHEN ISNULL(person_c_oksm,'')<>'' THEN REPLACE(person_c_oksm,' ','') ELSE 'ERROR' END+'" '+
			'KATEG="'+CASE WHEN ISNULL(person_kateg,'')<>'' THEN REPLACE(person_kateg,' ','') ELSE 'ERROR' END+'" '+
			CASE WHEN ISNULL(person_fiopr,'')<>'' THEN
			'FIOPR="'+ISNULL(person_fiopr,'ERROR')+'" '
			ELSE '' END+
			CASE WHEN ISNULL(person_ss,'')<>'' THEN
			'SS="'+person_ss+'" '
			ELSE ''END+
		'/>'+
		CASE
			WHEN ISNULL(oldperson_fam,'')<>'' AND ISNULL(oldperson_im,'')<>'' AND ISNULL(oldperson_dr,'')<>'' THEN
		'<OLD_PERSON '+
			'FAM="'+REPLACE(oldperson_fam,' ','')+'" '+
			'IM="'+REPLACE(oldperson_im,' ','')+'" '+
			'OT="'+CASE WHEN ISNULL(oldperson_ot,'')<>'' THEN REPLACE(oldperson_ot,' ','') ELSE '' END+'" '+
			'DR="'+CONVERT(CHAR(10),REPLACE(oldperson_dr,' ',''),20)+'" '+
			'W="'+ISNULL(CONVERT(CHAR(1),oldperson_w),'ERROR')+'" '+
		'/>'
		ELSE ''
		END+
		'<ADDRES_G '+
			'BOMG="0" '+ 
			'SUBJ="'+CASE WHEN ISNULL(addres_g_subj,'')<>'' THEN  addres_g_subj ELSE 'ERROR' END+'" '+ 
			CASE WHEN ISNULL(addres_g_indx,'')<>'' THEN
			'INDX="'+REPLACE(addres_g_indx,' ','')+'" '
			ELSE ''END+
			'OKATO="'+CASE WHEN ISNULL(addres_g_okato,'')<>'' THEN REPLACE(addres_g_okato,' ','') ELSE 'ERROR' END+'" '+ 
			CASE WHEN ISNULL(ro.CAPTION,'')<>'' THEN
			'RNNAME="'+CASE WHEN ISNULL(ro.CAPTION,'')<>'' THEN ro.CAPTION ELSE 'ERROR' END+'" '
			ELSE '' END+
			'NPNAME="'+
					CASE WHEN ISNULL(so.CAPTION,'')<>'' THEN
					REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(so.CAPTION,'� ',''),'�.�� ',''),'�� ',''),'� ',''),'� ',''),'� ',''),'� ','')
					ELSE 'ERROR' END
			+'" '+
			'DREG="'+CASE WHEN ISNULL(addres_g_dreg,'')<>'' THEN CONVERT(CHAR(10),addres_g_dreg,20) ELSE 'ERROR' END+'" '+
			CASE WHEN ISNULL(addres_g_ul,'')<>'' THEN
			'UL="'+addres_g_ul+'" '
			ELSE '' END+
			CASE WHEN ISNULL(addres_g_dom,'')<>'' THEN
			'DOM="'+REPLACE(addres_g_dom,' ','')+'" '
			ELSE ''END+
			CASE WHEN ISNULL(addres_g_korp,'')<>'' THEN
			'KORP="'+REPLACE(addres_g_korp,' ','')+'" '
			ELSE ''END+
			CASE WHEN ISNULL(addres_g_kv,'')<>'' THEN
			'KV="'+REPLACE(addres_g_kv,' ','')+'" '
			ELSE ''END+
		'/>'+
		'<ADDRES_P '+ 
			'SUBJ="'+CASE WHEN ISNULL(addres_p_subj,'')<>'' THEN  REPLACE(addres_p_subj,' ','') ELSE 'ERROR' END+'" '+ 
			CASE WHEN ISNULL(addres_p_indx,'')<>'' THEN
			'INDX="'+REPLACE(addres_p_indx,' ','')+'" '
			ELSE ''END+
			'OKATO="'+CASE WHEN ISNULL(addres_p_okato,'')<>'' THEN REPLACE(addres_g_okato,' ','') ELSE 'ERROR' END+'" '+ 
			CASE WHEN ISNULL(ro.CAPTION,'')<>'' THEN
			'RNNAME="'+CASE WHEN ISNULL(ro.CAPTION,'')<>'' THEN ro.CAPTION ELSE 'ERROR' END+'" '
			ELSE ''END+
			'NPNAME="'+
			CASE WHEN ISNULL(so.CAPTION,'')<>'' THEN
				REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(so.CAPTION,'� ',''),'�.�� ',''),'�� ',''),'� ',''),'� ',''),'� ',''),'� ','')
			ELSE 'ERROR' END
			+'" '+
			CASE WHEN ISNULL(addres_p_ul,'')<>'' THEN
			'UL="'+REPLACE(addres_p_ul,' ','')+'" '
			ELSE '' END+
			CASE WHEN ISNULL(addres_p_dom,'')<>'' THEN
			'DOM="'+REPLACE(addres_p_dom,' ','')+'" '
			ELSE ''END+
			CASE WHEN ISNULL(addres_p_korp,'')<>'' THEN
			'KORP="'+REPLACE(addres_p_korp,' ','')+'" '
			ELSE ''END+
			CASE WHEN ISNULL(addres_p_kv,'')<>'' THEN
			'KV="'+REPLACE(addres_p_kv,' ','')+'" '
			ELSE ''END+
		'/>'+
		'<VIZIT '+
			'DVIZIT="'+CONVERT(CHAR(10),GETDATE(),20)+'" '+
			'METHOD="'+CASE WHEN ISNULL(visit_method,'')<>'' THEN REPLACE(visit_method,' ','') ELSE 'ERROR' END + '" '+
			'PETITION="0" '+
			'RSMO="'+CASE WHEN visit_rsmo IS NOT NULL THEN REPLACE(CONVERT(CHAR(1),visit_rsmo),' ','') ELSE 'ERROR' END + '" '+
			'RPOLIS="'+CASE WHEN visit_rpolis IS NOT NULL THEN REPLACE(CONVERT(CHAR(1),visit_rpolis),' ','') ELSE 'ERROR' END+'" '+
			'FPOLIS="'+CASE WHEN visit_fpolis IS NOT NULL THEN REPLACE(CONVERT(CHAR(1),visit_fpolis),' ','') ELSE 'ERROR' END+'" '+
		'/>'+
		'<INSURANCE '+
			'TER_ST="'+CASE WHEN ISNULL(insurance_ter_st,'')<>'' THEN REPLACE(insurance_ter_st,' ','') ELSE 'ERROR' END + '" '+
			'OGRNSMO="'+CASE WHEN ISNULL(insurance_ogrnsmo,'')<>'' THEN REPLACE(insurance_ogrnsmo,' ','') ELSE 'ERROR' END+'" '+
			'ERP="'+CASE WHEN insurance_erp IS NOT NULL THEN CONVERT(CHAR(1),insurance_erp) ELSE 'ERROR' END+'" '+
			'ENP="'+CASE WHEN ISNULL(insurance_enp,'')<>'' THEN REPLACE(insurance_enp,' ','') ELSE 'ERROR' END+'" '+
		'>'+
			'<POLIS '+
				'VPOLIS="'+CASE WHEN polis_vpolis IS NOT NULL THEN CONVERT(CHAR(1),polis_vpolis) ELSE 'ERROR' END+'" '+
				'SPOLIS="'+REPLACE(ISNULL(polis_spolis,''),' ','') +'" '+
				'NPOLIS="'+CASE WHEN ISNULL(polis_npolis,'')<>'' THEN REPLACE(polis_npolis,' ','') ELSE 'ERROR' END+'" '+
				'DBEG="'+CONVERT(CHAR(10),GETDATE(),20)+'" '+
				CASE WHEN ISNULL(polis_dend,'')<>'' THEN
				'DEND="'+CONVERT(CHAR(10),REPLACE(polis_dend,' ',''),20)+'" '
				ELSE '' END+
				CASE WHEN ISNULL(polis_dstop,'')<>'' THEN
				'DSTOP="'+REPLACE(CONVERT(CHAR(10),polis_dstop,20),' ','')+'" '
				ELSE '' END+
			'/>'+
		'</INSURANCE>'+
		'<DOC_LIST>'+
			'<DOC '+
				'DOCTYPE="'+CASE WHEN ISNULL(doc_type,'')<>'' THEN doc_type ELSE 'ERROR' END+'" '+
				'DOCSER="'+CASE WHEN ISNULL(doc_ser,'')<>'' THEN 
					CASE WHEN ISNULL(doc_type,'')='14' THEN
						STUFF(REPLACE(doc_ser,' ',''),3,0,' ')
						ELSE doc_ser 
					END
					ELSE 'ERROR' END+'" '+
				'DOCNUM="'+CASE WHEN ISNULL(doc_num,'')<>'' THEN REPLACE(doc_num,' ','') ELSE 'ERROR' END+'" '+
				'DOCDATE="'+CASE WHEN ISNULL(doc_date,'')<>'' THEN CONVERT(CHAR(10),ISNULL(doc_date,''),20) ELSE 'ERROR' END+'" '+
				CASE WHEN ISNULL(polis_dstop,'')<>'' THEN
				'DOCEXP="'+ISNULL(CONVERT(VARCHAR(10),doc_exp,20),'')+'" '
				ELSE '' END+
				'NAME_VP="'+CASE WHEN ISNULL(doc_name_vp,'')<>'' THEN doc_name_vp ELSE 'ERROR' END+'" '+
			'/>'+
		
		--	'<DOC '+
		--		'DOCTYPE="'+ISNULL(REPLACE(st.type_doc_right_rf,' ',''),ISNULL(REPLACE(st.type_doc_right_rf_before_changing,' ',''),''))+'" '+
		--		'DOCSER="'+ISNULL(st.serial_doc_right_rf,ISNULL(REPLACE(st.serial_doc_right_rf_before_changing,' ',''),''))+'" '+
		--		'DOCNUM="'+
		--			CASE
		--				WHEN ISNULL(REPLACE(st.type_doc_right_rf,' ',''),ISNULL(REPLACE(st.type_doc_right_rf_before_changing,' ',''),''))='23' and LEN(REPLACE(ISNULL(REPLACE(st.number_doc_right_rf,' ',''),ISNULL(REPLACE(st.number_doc_right_rf_before_changing,' ',''),'')),'/',''))=9 THEN
		--					STUFF(STUFF(REPLACE(ISNULL(REPLACE(st.number_doc_right_rf,' ',''),ISNULL(REPLACE(st.number_doc_right_rf_before_changing,' ',''),'')),'/',''),4,0,'/'),9,0,'/')
		--				ELSE 
		--					ISNULL(REPLACE(st.number_doc_right_rf,' ',''),ISNULL(REPLACE(st.number_doc_right_rf_before_changing,' ',''),''))
		--			END
		--			+'" '+
		--		'DOCDATE="'+ISNULL(CONVERT(CHAR(10),st.date_issue_doc_right_rf,20),ISNULL(CONVERT(VARCHAR(10),st.date_issue_doc_right_rf_before_changing,20),''))+'" '+ 
		--		'DOCEXP="'+ISNULL(CONVERT(VARCHAR(10),st.date_expire_doc_right_rf,20),ISNULL(CONVERT(VARCHAR(10),st.date_expire_doc_right_rf_before_changing,20),ISNULL(CONVERT(VARCHAR(10),st.date_end_doc_residence_permit,20),CONVERT(VARCHAR(10),@dstopdef,20))))+'" '+
		--		'NAME_VP="'+REPLACE(ISNULL(st.issued_doc_right_rf,ISNULL(st.issued_doc_right_rf_before_changing,'')),'"','')+'" '+
		--	'/>'
			
		--END+
		--'</DOC_LIST>'+
		--CASE 
		--	WHEN p.assured_nationality='RUS' AND st.document_number_before_changing IS NOT NULL AND @TIP_OP<>'�060' THEN
		--	'<OLDDOC_LIST>'+
		--		'<OLD_DOC '+
		--			'DOCTYPE="'+REPLACE(ISNULL(st.id_document_type_before_changing,''),' ','')+'" '+
		--			'DOCSER="'+
		--			CASE 
		--				WHEN ISNULL(st.id_document_type_before_changing,0) = 14 AND LEN(ISNULL(st.document_series_before_changing,''))=4 THEN 
		--					SUBSTRING(ISNULL(st.document_series_before_changing,''),1,2)+' '+SUBSTRING(ISNULL(st.document_series_before_changing,''), 3,2)
		--				ELSE ISNULL(st.document_series_before_changing,'')
		--			END+
		--			+'" '+
		--			'DOCNUM="'+REPLACE(ISNULL(st.document_number_before_changing,''),' ','')+'" '+
		--			'DOCDATE="'+CONVERT(CHAR(10),ISNULL(st.document_date_before_changing,''),20)+'" '+
		--	CASE
		--		WHEN p.assured_nationality<>'RUS' AND p.document_date_expire IS NOT NULL THEN 
		--			'DOCEXP="'+ISNULL(CONVERT(VARCHAR(10),p.document_date_expire,20),'')+'" '
		--		ELSE ''
		--	END+
		--	'/>'+
		--	'</OLDDOC_LIST>'
		--	ELSE ''
		--END+
	'</DOC_LIST>'+
	'</OP>' --AS XML

FROM dbo.discharge d(NOLOCK)															
left outer join srz..OKATO so on so.CODE=d.addres_g_okato
left outer join srz..OKATO ro on ro.CODE=left(d.addres_g_okato,5)+'000000'
left outer join srz..people sp on sp.q='46001' and sp.ID=d.id and sp.DSTOP is null
where Q='46001'
--and visit_dvizit between @beg and @end
--left outer join lists l (nolock) on l.n_rec=d.n_rec
--WHERE insurance_enp='4654340875000110'
--left outer join users u (nolock) on u.user_id=l.user_id
--left outer join srz..PEOPLE w1(nolock) on n.polis_number=w1.ENP 
--left outer join srz..PEOPLE w2 (nolock) on w1.NPOL IS NULL AND (w2.NPOL=n.polis_number) 
--LEFT OUTER JOIN srz..PEOPLE w3(nolock) on w2.NPOL IS NULL AND w3.NPOL=replace(n.polis_blank_serial+n.polis_blank_number,' ','')

--WHERE
	--l.parcel_id in('B98323DC-956A-4325-AD0B-121532ED6E9D','11A99B51-5F68-4109-8E1B-5BCA9C11D7CC','A4504CA3-01C2-4E59-8631-1BB92E831526','5624610E-0FFE-41E8-9A5C-E803724E9A16')
	--and visit_dvizit between @beg and @end
--SELECT '</OPLIST>'
