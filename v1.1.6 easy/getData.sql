--use srz
select top(1000) 
ISNULL(sp.npol,'') as npol
,ISNULL(sp.FAM,'') as fam
,ISNULL(sp.IM,'') as im
,ISNULL(sp.OT,'') as ot
,ISNULL(CONVERT(CHAR(10),sp.DR,104),'') as dr
,CASE 
	WHEN sp.W=1 THEN '�'
	WHEN sp.W=2 THEN '�'
	ELSE ''
END as w
,CASE 
	WHEN sp.CN='RUS' THEN '������'
	ELSE ISNULL(sp.CN,'')
END as cn
,CASE
	WHEN sp.KATEG='1' THEN '����������'
	WHEN sp.KATEG='6' THEN '�� ����������'
	ELSE ''
 END as kateg
,CASE WHEN sp.DOCTP='14' THEN '������� ��' 
	  WHEN sp.DOCTP='3' THEN '������������� � ��������'
	  ELSE '' 
 END as doctp
,CASE WHEN sp.DOCTP='14' OR sp.DOCTP='3' THEN REPLACE(ISNULL(DOCS,''),' ','') ELSE '' END as docs
,CASE WHEN sp.DOCTP='14' OR sp.DOCTP='3' THEN ISNULL(DOCN,'') ELSE '' END as docn
,CASE WHEN sp.DOCTP='14' OR sp.DOCTP='3' THEN ISNULL(CONVERT(CHAR(10),DOCDT,104),'') ELSE '' END as docdt
,CASE 
	WHEN sp.DOCTP='14' THEN ISNULL(DOCORG,'�� �7 ��� � ������') 
	WHEN sp.DOCTP='3' THEN ISNULL(DOCORG,'���. ���� ���. ��������� ���. �. ������ ������� ���')
	ELSE ''
 END as docorg
,ISNULL(sp.MR,'') as mr
,CASE WHEN ISNULL(ro.CAPTION,'')<>'' THEN ro.CAPTION ELSE '' END as rnname
,CASE WHEN ISNULL(so.CAPTION,'')<>'' THEN
					REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(so.CAPTION,'� ',''),'�.�� ',''),'�� ',''),'� ',''),'� ',''),'� ',''),'� ','')
					ELSE '' END as np
,ISNULL(sp.UL,'') as ul
,ISNULL(sp.DOM,'') as dom
,ISNULL(sp.KOR,'') as kor
,ISNULL(sp.KV,'') as kv
,ISNULL(CONVERT(CHAR(10),sp.DMJ,104),'') as dmj
,ISNULL((select VALUE from dbo.fn_Split((SELECT FIOPR FROM SRZ..PEOPLE WHERE ENP=sp.enp),' ') WHERE position=1),'') as prfam
,ISNULL((select VALUE from dbo.fn_Split((SELECT FIOPR FROM SRZ..PEOPLE WHERE ENP=sp.enp),' ') WHERE position=2),'') as prim
,ISNULL((select VALUE from dbo.fn_Split((SELECT FIOPR FROM SRZ..PEOPLE WHERE ENP=sp.enp),' ') WHERE position=3),'') as prot
,REPLACE(REPLACE(ISNULL(sp.SS,''),' ',''),'-','') as ss
,ISNULL(sp.PHONE,'') as phone
,sp.q as q
--, * 
from people sp
left outer join srz..OKATO so on so.CODE=sp.rn
left outer join srz..OKATO ro on ro.CODE=left(sp.rn,5)+'000000'
where sp.enp=''